<?php

namespace Drupal\tmgmt_powerling\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt\TMGMTException;
use Drupal\tmgmt_powerling\Plugin\tmgmt\Translator\PowerlingTranslator;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Handles callbacks for tmgmt_powerling module.
 */
class PowerlingController extends ControllerBase {

  /**
   * Logger logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new instance of PowerlingController.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger.
   */
  public function __construct(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')->get('tmgmt_powerling')
    );
  }

  /**
   * Callback for file status update.
   *
   * @param \Drupal\tmgmt\JobItemInterface $tmgmt_job_item
   *   Job item.
   * @param string $order_id
   *   Order ID.
   * @param string $file_id
   *   File ID.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response.
   */
  public function fileCallback(JobItemInterface $tmgmt_job_item, $order_id, $file_id) {
    if (!$tmgmt_job_item->getTranslatorPlugin() instanceof PowerlingTranslator) {
      $this->logger->warning('Invalid parameters when receiving remote response for job item %id', ['%id' => $tmgmt_job_item->id()]);
      throw new NotFoundHttpException();
    }

    $translator = $tmgmt_job_item->getTranslator();
    /** @var \Drupal\tmgmt_powerling\Plugin\tmgmt\Translator\PowerlingTranslator $translatorPlugin */
    $translatorPlugin = $translator->getPlugin();

    try {
      $translatorPlugin->updateTranslation($tmgmt_job_item, $order_id, $file_id);
    }
    catch (TMGMTException $tmgmtException) {
      $tmgmt_job_item->addMessage($tmgmtException->getMessage());
    }
    catch (\Exception $e) {
      watchdog_exception('tmgmt_powerling', $e);
      return new Response(NULL, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    return new Response(NULL, Response::HTTP_OK);
  }

  /**
   * Callback function for order status update.
   *
   * @param \Drupal\tmgmt\JobInterface $tmgmt_job
   *   Translation job.
   * @param string $order_id
   *   Order ID.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response.
   */
  public function orderCallback(JobInterface $tmgmt_job, $order_id) {
    if (!$tmgmt_job->getTranslatorPlugin() instanceof PowerlingTranslator) {
      $this->logger->warning('Invalid parameters when receiving remote response for job %id', ['%id' => $tmgmt_job->id()]);
      throw new NotFoundHttpException();
    }

    $translator = $tmgmt_job->getTranslator();
    /** @var \Drupal\tmgmt_powerling\Plugin\tmgmt\Translator\PowerlingTranslator $translatorPlugin */
    $translatorPlugin = $translator->getPlugin();

    $order = $translatorPlugin->getOrder($translator, $order_id);

    if ($order['status'] == 'canceled') {
      $translatorPlugin->abortJob($tmgmt_job);
    }
    else {
      $tmgmt_job->addMessage('Order (@order_id) status has been changed to @status.', [
        '@order_id' => $order_id,
        '@status' => $order['status'],
      ]);
    }

    return new Response(NULL, Response::HTTP_OK);
  }

}
