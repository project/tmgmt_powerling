# Powerling translation provider for Translation Management Tool (TMGMT)

## Overview

The Powerling translation provider for Translation Management Tool allows you to submit your content directly to Powerling. The content is directly received by our Project Manager(s) & Human Translators. When the translation is done, you will view it in your Drupal CMS. Only one more click and your translation will go live!
This solution exists to streamline your translation process in a user-friendly interface.
Features

- All translation is completed using your CMS/ website interface. No more import/export.
- It is possible to translate any type of content from your Drupal website (entity field, taxonomy, etc...)
- Translated content is automatically put back to its original place.

## Requirements

- For this plugin to work, you will first have to install and enable the TMGMT module as well as its extension. You will customize the module to your translation needs, selecting the type of content you want to have translated (for example, entity field, taxonomy, etc...)
- Contact Powerling to get information and an estimate for your translations. The IT team will provide you with a token to connect the plugin to our tool and to our human translators.

## Compatibility

This plugin is compatible with Drupal version 8.x (no support for earlier versions)

## About Powerling

Powerling, Smart Language Solutions is a content-oriented company founded in 1983 and has been established as a trusted, versatile, and comprehensive language service provider. Specializing in the sectors of life sciences, law & finance, IT & marketing, and manufacturing & distribution, Powerling provides solutions in 75 plus languages. Our goal is to deliver excellent translations to our clients every time.

For more information about plugin configuration and request a token: https://powerling.com/tools-technology/cms-connectors/drupal
For more information about Powerling: https://powerling.com/
